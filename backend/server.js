const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');
const passport = require('passport');
const session = require('express-session');
const cors = require('cors');
const logger = require('morgan');

const app = express();
app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(session({secret:"secret key"}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/api', router);



app.listen(8080, () => console.log("Server is running on http://localhost:8080"));

module.exports = app;