module.exports = (sequelize, DataTypes) => {
    return sequelize.define("order", {
        clientEmail: {
            type: DataTypes.STRING
        },
        productList: {
            type: DataTypes.TEXT,
            get: function () {
                return JSON.parse(this.getDataValue('productList'));
            },
            set: function (value) {
                this.setDataValue('productList', JSON.stringify(value));
            }
        },
        totalSum: {
            type: DataTypes.INTEGER
        },
        paymentMethod: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING
        }
    })
};