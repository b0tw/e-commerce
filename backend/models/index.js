const db = require('../config').db;

const Client = db.import('./client');
const Product = db.import('./product');
const Brand = db.import('./brand');
const Category = db.import('./category');
const Subcategory = db.import('./subcategory');
const Order = db.import('./order');
module.exports = {
    Client,
    Product,
    Brand,
    Category,
    Subcategory,
    Order,
    connection:db
};

