module.exports = (sequelize, DataTypes) => {
    return sequelize.define("client", {
        firstName: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        lastOrder: {
            type: DataTypes.STRING
        }
    })
};