module.exports = (sequelize, DataTypes) => {
    return sequelize.define("product", {
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.INTEGER
        },
        stock: {
            type: DataTypes.INTEGER
        },
        description: {
            type: DataTypes.STRING
        }
    }, { timestamps: false })
};