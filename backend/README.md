 Backend documentation
======
## Route list 
### APIs (/api)
#### Authentification (/auth)
| Method | Route | Description |
|:---:|:---:|:---:|
|POST|/login|logins a user|
|GET|/logout|logouts an authentificated user
#### Client (/client)
| Method | Route | Description |
|:---:|:---:|:---:|
|POST|/register|registers an user|
|POST|/update|updates an user profile|
#### Product (/product)
| Method | Route | Description |
|:---:|:---:|:---:|
|POST|/buy|makes the buy order|
|POST|/register|adds a product to the stock|
#### Other (/other)- only for development
| Method | Route | Description |
|:---:|:---:|:---:|
|GET|/reset|resets the database|
## Models
#### Client
| Field | Type | Description |
|:---:|:---:|:---:|
|id|integer|primary key|
|firstName|string|first name|
|lastName|string|last name|
|email|string|email|
|password|string|password|
|lastOrder|string|the date and the products bought last time the user orded|
#### Product 
| Field | Type | Description |
|:---:|:---:|:---:|
|id|integer|primary key|
|name|string|name|
|price|integer|price|
|stock|integer|describes how many units are in stock|
|description|string|description of the product|
#### Order
| Field | Type | Description |
|:---:|:---:|:---:|
|id|integer|primary key|
|clientEmail|string|the email of the client|
|product list|text|JSON object with the id and the number of each product ordered|
|totalSum|integer|the sum|
|paymentMethod|string|payment method|
|status|string|the status of the order; can be pending/ accepted/ rejected|

The database was modeled using [Sequelize](https://sequelize.org/master/ "Sequelize Main Page").
