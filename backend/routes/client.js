const express = require('express');
const router = express.Router();
const client = require('../controllers').Client;
const auth = require('../controllers').Auth;

router.post('/register', client.register);
router.post('/update', auth.isAuthenticated, client.update);
module.exports = router;