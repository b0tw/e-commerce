const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../controllers').Auth;

router.post('/login', passport.authenticate('local'), auth.login);
router.get('/logout', auth.isAuthenticated, auth.logout);
router.get('/client', auth.isAuthenticated, auth.client);
module.exports = router;