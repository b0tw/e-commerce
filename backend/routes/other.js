const express = require('express');
const router= express.Router();
const otherController = require('../controllers').Other;

router.get('/',otherController.reset);

module.exports = router;
