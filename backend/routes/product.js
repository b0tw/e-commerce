const express = require('express');
const router = express.Router();
const product = require('../controllers').Product;
const auth = require('../controllers').Auth

router.post('/buy', auth.isAuthenticated, product.buy);
router.post('/register', product.register);
module.exports = router;