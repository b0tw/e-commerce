const express = require('express');
const router = express.Router();
const otherRouter = require('./other');
const authRouter = require('./auth');
const clientRouter = require('./client');
const productRouter = require('./product');

router.use('/other', otherRouter);
router.use('/auth', authRouter);
router.use('/client', clientRouter);
router.use('/product', productRouter);

module.exports = router;
