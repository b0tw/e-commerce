const bcrypt = require('bcrypt');
const Client = require('../models').Client;
const saltRounds = 10;

const controller = {
    register: async (req, res) => {
        let errors = [];
        firstName = req.body.firstName;
        lastName = req.body.lastName;
        email = req.body.email;
        password = req.body.password;
        address = req.body.address;

        const existantMail = await Client.findOne({ where: { email }, attributes: ['email'] });

        if (!firstName.match("^[A-Z]*[a-zA-Z-' ]+$")) {
            errors.push("Invalid firstname");
        }
        else if (!lastName.match("^[A-Z]*[a-zA-Z-' ]+$")) {
            errors.push("Invalid lastname");
        }
        else if (!email.match("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")) {
            errors.push("Invalid email address");
            //using practical RFC 2822 implementation 
        }
        else if (existantMail) {
            errors.push("Email already exists");
        }
        else if (!password.match("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,20}$")) {
            errors.push("Invalid password. The password must contain at least a capital letter, a number and be between 6 and 20 characters long");
        }

        if (errors.length == 0) {
            const client = {
                firstName,
                lastName,
                email,
                password,
                address
            };
            bcrypt.genSalt(saltRounds, (err, salt) => {
                bcrypt.hash(client.password, salt, (err, hash) => {
                    client.password = hash;
                    Client.create(client);
                });
            });
            res.status(200).send({ message: "Register was successful" });
        } else {
            res.status(422).send(errors);
        }
    },
    update: async (req, res) => {
        let errors = [];
        const email = req.body.email;
        const password = req.body.password;
        const address = req.body.address;
        const existantMail = await Client.findOne({ where: { email }, attributes: ["email"] });
        const user = await req.user;
        const client = {};

        if (!email.match("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")) {
            errors.push("Invalid email address");
        }
        else if (existantMail) {
            errors.push("Email address already exists");
        }
        else if (email == user.email) {
            errors.push("The same email address was entered");
        }
        if (!password.match("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,20}$")) {
            errors.push("Invalid password. The password must contain at least a capital letter, a number and be between 6 and 20 characters long")
        }
        bcrypt.compare(password, user.password, (err, result) => {
            if (result) {
                errors.push("The same password was entered");
            }
        });

        if (email) {
            client.email = email;
        }
        if (password) {
            bcrypt.genSalt(saltRounds, (err, salt) => {
                bcrypt.hash(client.password, salt, (err, hash) => {
                    client.password = hash;
                });
            });
        }
        if (address) {
            client.address = address;
        }
        if (errors.length == 0) {
            console.log(user.id);
            Client.findByPk(user.id).then(modifyingClient => { modifyingClient.update(client) },
                () => { res.status(500).send({ message: "Something wrong happened" }) });
        }
        else res.status(422).send(errors);
    }
};

module.exports = controller;