const Product = require('../models').Product;
const Order = require('../models').Order;

const controller = {
    buy: async (req, res) => {
        const user = await req.user;
        const order = {
            clientEmail: user.email,
            productList: req.body.productList,
            totalSum: req.body.totalSum,
            paymentMethod: req.body.paymentMethod,
            status: 'pending'
        }

        for (let i = 0; i < order.productList.length; i++) {
            Product.findOne(
                { where: { id: order.productList[i].id } }).then(
                    product => {
                        return product.decrement('stock', { by: order.productList[i].quantity }
                        )
                    },
                    () => { res.status(500).send({ message: "Something bad happened with the products" }) });
        }
        Order.create(order).then(() => { res.status(200).send("Order successfully made") },
            () => { res.status(500).send({ message: "Failed to create order" }) });
    },
    register: (req, res) => {
        const product = {
            name: req.body.name,
            price: req.body.price,
            stock: req.body.stock,
        }
        Product.create(product).then(() => { res.status(200).send({ message: "Product successfully registered" }) },
            () => { res.status(500).send({ message: "Couldn't register product" }) });
    }
}

module.exports = controller;