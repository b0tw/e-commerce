const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const Client = require('../models').Client;

passport.use(new LocalStrategy(
    { usernameField: "email", passwordField: "password" },
    async (username, password, done) => {
        const client = await Client.findOne({ where: { email: username }, raw: true }).catch(() => {
            return done(null, false, { message: "Database error" })
        });
        if (client) bcrypt.compare(password, client.password, (err, result) => {
            if (result) {
                return done(null, client);
            } else {
                return done(null, false, { message: "Incorrect password" });
            }
        });
        else {
            return done(null, false, { message: "Incorrect email" });
        }

    }
));

passport.serializeUser((client, done) => {
    done(null, client.id);
});

passport.deserializeUser(async (id, done) => {
    const client = Client.findOne({ where: { id } });

    done(null, client);
});
const controller = {
    login: (req, res) => {
        res.status(200).send({ message: "Login was successful" });
    },
    isAuthenticated: (req, res, next) => {
        if (!req.isAuthenticated()) {
            res.status(401).send({ message: "Unathorized" });
        } else {
            next();
        }
    },
    logout: (req, res) => {
        req.logout();
        res.status(200).send({ message: "Logout successful" });
    },
    client: async (req, res) => {
        const user = await req.user;
        res.send(user)
    }
};

module.exports = controller;