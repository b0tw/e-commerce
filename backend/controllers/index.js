const Auth = require('./auth');
const Client = require('./client');
const Product = require('./product');
const Other = require('./other');


const controller = {
    Auth,
    Client,
    Product,
    Other
};

module.exports = controller;
