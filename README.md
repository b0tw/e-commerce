# e-commerce website
Online shop.
Backend is done using Node.js with Express framework.
Frontend uses Vue.js with Quasar as framework.
* [Backend docs](/backend/README.md)
* [Frontend docs](/frontend/README.md)